import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.Test;

class TP02E01BaseTest {

	@Test
	public void testExtraitValeurs() {
		int[] test0 = new int[0];
		int[] test1 = { 49, 121, 9, 9, 3, 10, 8, 2 }, 
		test0ETtest1 = new int[0];

		int[] test2 = { 2, 3, 4, 6, 8, 9, 12, 18, 21, 33 }, test2ResultTrue = { 2, 4, 6, 8, 12, 18 };
		int[] test2ResultFalse = { 3, 9, 21, 33 };

		int[] test3 = { 121, 24, 36, 40, 15, 12, 31, 0, 2, 1 }, test3ResultTrue = { 24, 36, 40, 12, 0, 2 };
		int[] test3ResultFalse = { 121, 15, 31, 1 };

		int[] test4 = { 2, 6, 45, 9, 3, 7, 58, 21, 49, 42 }, test4ResultTrue = { 2, 6, 58, 42 };
		int[] test4ResultFalse = { 45, 9, 3, 7, 21, 49 };

		assertArrayEquals(test0ETtest1, TP02E01Base.extraitValeurs(test0, true));
		assertArrayEquals(test2ResultTrue, TP02E01Base.extraitValeurs(test2, true));
		assertArrayEquals(test2ResultFalse, TP02E01Base.extraitValeurs(test2, false));
		assertArrayEquals(test3ResultTrue, TP02E01Base.extraitValeurs(test3, true));
		assertArrayEquals(test3ResultFalse, TP02E01Base.extraitValeurs(test3, false));
		assertArrayEquals(test4ResultTrue, TP02E01Base.extraitValeurs(test4, true));
		assertArrayEquals(test4ResultFalse, TP02E01Base.extraitValeurs(test4, false));

	}
	@Test
	public void testTriSel() {
		
		int[] test0 = new int[2], Result = new int[2];

		int[] test1 = new int[5], Result1 = { 0, 0, 0, 0, 0,};

		
		int[] test2 = { 24, 9, 18, 12, 3, 6 }, Result2 = { 3, 6, 9, 12, 18, 24 };

		int[] test3 = { 2, 21, 3, 9, 7, 7 }, Result3 = { 2, 3, 7, 7, 9, 21 };

		int[] test4 = { 33, 999, 1, 100, 4, 8 }, Result4 = { 1, 4, 8, 33, 100, 999 };

		TP02E01Base.triSel(test0);
		assertArrayEquals(Result, test0);

		TP02E01Base.triSel(test1);
		assertArrayEquals(Result1, test1);

		TP02E01Base.triSel(test2);
		assertArrayEquals(Result2, test2);

		TP02E01Base.triSel(test3);
		assertArrayEquals(Result3, test3);

		TP02E01Base.triSel(test4);
		assertArrayEquals(Result4, test4);
	}

	
}
